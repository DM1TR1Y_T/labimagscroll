﻿using labImagScroll.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImagScroll
{
    public partial class Form1 : Form
    {
        private Bitmap b;
        private Point curPoint;
        private Point startPoint;
        private ResourceSet resourceSet;
        private IDictionaryEnumerator enumerator;
        private ResourceManager resourceManager;

        public int ZoomDelta { get; private set; } = 100;
        public Form1()
        {
            InitializeComponent();
            resourceManager = new ResourceManager(typeof(Resources));
            resourceSet = resourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            enumerator = resourceSet.GetEnumerator();
            enumerator.MoveNext();

            Bitmap myImage = (Bitmap)resourceManager.GetObject(enumerator.Key.ToString());
            b = new Bitmap(myImage);

            pxImage.Paint += (s, e) => e.Graphics.DrawImage(b, curPoint);
            pxImage.MouseDown += (s, e) => startPoint = e.Location;
            pxImage.MouseMove += PxImage_MouseMove;
            pxImage.MouseWheel += PxImage_MouseWheel;
            this.KeyDown += Fm_KeyDown;
            this.Text += " : F1 - следующая картинка";
            

            //HW
            //по кнопке F1 переключаться на следующее изображение из ресурсов
            //по ролику мыши изменять масштаб изображения под курсором мыши
            //добавить ограничения на ZoomDelta и т.п.
        }


        private void LoadNextImage()
        {
            if (enumerator.MoveNext())
            {
                Bitmap myImage = (Bitmap)resourceManager.GetObject(enumerator.Key.ToString());
                b = new Bitmap(myImage);
                Refresh();
            } else
            {
                enumerator.Reset();
                enumerator.MoveNext();
                Bitmap myImage = (Bitmap)resourceManager.GetObject(enumerator.Key.ToString());
                b = new Bitmap(myImage);
                Refresh();
            }
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    LoadNextImage();
                    break;
            }
        }
        private void PxImage_MouseWheel(object sender, MouseEventArgs e)
        {
            if (e.Delta != 0)
            {
                if (e.Delta <= 0)
                {
                    if (pxImage.Width < 100)
                    {
                        return;
                    }
                    ZoomDelta -= e.Delta < 0 ? 2 : -2;
                    pxImage.Width -= Convert.ToInt32(pxImage.Width * ZoomDelta / 1000);
                    pxImage.Height -= Convert.ToInt32(pxImage.Height * ZoomDelta / 1000);
                    pxImage.Location = new Point(Cursor.Position.X - pxImage.Width / 2, Cursor.Position.Y - pxImage.Height / 2);
                    pxImage.SizeMode = PictureBoxSizeMode.StretchImage;
                }
                else
                {
                    if (pxImage.Width > 1000)
                    {
                        return;
                    }
                    ZoomDelta += e.Delta < 0 ? 2 : +2;
                    pxImage.Width += Convert.ToInt32(pxImage.Width * ZoomDelta / 1000);
                    pxImage.Height += Convert.ToInt32(pxImage.Height * ZoomDelta / 1000);
                    pxImage.Location = new Point(Cursor.Position.X - pxImage.Width / 2, Cursor.Position.Y - pxImage.Height / 2);

                    pxImage.SizeMode = PictureBoxSizeMode.StretchImage;
                }
                pxImage.Invalidate();
                RefreshStatus();
            }
        }

        private void RefreshStatus()
        {
            laStatus.Text = $"startPoint={startPoint}, curPoint={curPoint}, ZoomDelta={ZoomDelta}";
        }

        private void PxImage_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                curPoint.X += e.X - startPoint.X;
                curPoint.Y += e.Y - startPoint.Y;
                startPoint = e.Location;
                pxImage.Invalidate();
                RefreshStatus();
            }  }
    }
}
